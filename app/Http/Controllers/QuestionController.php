<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class QuestionController extends Controller
{
    public function create() {
    	return view('questions.create');
    }

    public function store(Request $request) {
    	//dd($request->all());
    	$request->validate([
    		'title' => 'required|unique:posts',
    		'body'	=> 'required'
    	]);
    	$query = DB::table('posts')->insert([
    		"title" 		=> $request["title"],
    		"body"			=> $request["body"]
    	]);

    	return redirect('/questions')->with('success', 'Post Berhasil Disimpan!');
    }

    public function index() {
    	$posts = DB::table('posts')->get();
    	//dd($posts);
    	return view('questions.index', compact('posts'));
    }

    public function show($id) {
    	$post = DB::table('posts')->where('id', $id)->first();
    	return view('questions.show', compact('post'));
    }
}
